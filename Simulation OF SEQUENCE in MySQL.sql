DROP DATABASE `Testeo`;

CREATE DATABASE `Testeo`;

USE `Testeo`;


CREATE TABLE `SEQUENCE` (
    `name` VARCHAR ( 100 ) NOT NULL PRIMARY KEY,
    `INCREMENT` INT ( 11 ) NOT NULL DEFAULT 1,
    `MIN_VALUE` INT ( 11 ) NOT NULL DEFAULT 1,
    `MAX_VALUE` BIGINT ( 20 ) NOT NULL DEFAULT 9223372036854775807,
    `CUR_VALUE` BIGINT ( 20 ) DEFAULT 1,
    `CYCLE` BOOLEAN NOT NULL DEFAULT FALSE 
) ENGINE = MyISAM;


INSERT INTO `SEQUENCE` ( `name`, `INCREMENT`, `MIN_VALUE`, `MAX_VALUE`, `CUR_VALUE` )
VALUES
    ( 'Secuencia1', 1, 1, 100, 1 ); /*Create The sequence*/


DELIMITER $$
CREATE FUNCTION `NEXTVAL` 
( 
    `sequence_name` VARCHAR ( 100 ) 
) 
RETURNS BIGINT NOT DETERMINISTIC 
BEGIN
    DECLARE
        `CUR_VAL` BIGINT;
    SELECT
        `CUR_VALUE` INTO `CUR_VAL` 
    FROM
        `SEQUENCE` 
    WHERE
        `name` = `sequence_name`;
    IF
        ( `CUR_VAL` IS NOT NULL ) THEN/*verifica dato no nulo*/
            UPDATE `SEQUENCE` 
            SET `CUR_VALUE` =
        IF
            (
                 ( `CUR_VALUE` + `INCREMENT` ) > `MAX_VALUE`  
                OR 
                 ( `CUR_VALUE` + `INCREMENT` ) < `MIN_VALUE` ,
            IF
                ( 
                    `CYCLE` = TRUE, 
                        IF ( 
                            ( `CUR_VALUE` + `INCREMENT` ) > `MAX_VALUE`, /*si el cycle es true, le asigna el min en caso de superar el maximo, de lo contrario le asigna el maximo*/
                            `MIN_VALUE`, 
                            `MAX_VALUE` 
                        ), 
                        NULL /*nulo si supera el minimo o maximo y el cycle is false*/
                ),
            `CUR_VALUE` + `INCREMENT` /*si no supera al maximo o minimo asigna este*/
        ) 
        WHERE
            `name` = `sequence_name`;     
    END IF;
    RETURN `CUR_VAL`;  
END;
$$ 
DELIMITER ;

CREATE TABLE `testSequence`
(
    `id` BIGINT NOT NULL PRIMARY KEY,
    `text` VARCHAR ( 10 ) NULL
)
ENGINE = InnoDB DEFAULT CHARSET = latin1 ;

DELIMITER $$

CREATE TRIGGER `insertTestSequence`
BEFORE INSERT ON `testSequence`
FOR EACH ROW
  IF (new.`id` IS NULL)
  THEN
    SET new.`id` = `NEXTVAL`('Secuencia1');
  END IF;
$$ 
DELIMITER ;


INSERT INTO `testSequence` ( `text` ) VALUES ( 'Hello World' ),( 'Bye World' );

SELECT * FROM `testSequence`;
#Copyright (c) DarKPhuRioN